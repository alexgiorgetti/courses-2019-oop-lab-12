﻿using System;
using System.Collections.Generic;

namespace IndexerGenerics
{
    public class Map2D<TKey1, TKey2, TValue> : IMap2D<TKey1, TKey2, TValue>
    {
        /*
         * Suggerimento: come struttura dati interna per la memorizzazione dei valori della mappa
         * si suggerisce di utilizzare un Dictionary generico contenente come chiave una tupla di due
         * valori (le chiavi della mappa) e come valore il valore della mappa.
         * 
         * Esempio:
         * private IDictionary<Tuple<TKey1,TKey2>,TValue> values;
         */

        private IDictionary<Tuple<TKey1, TKey2>, TValue> values;

        public Map2D()
        {
            values = new Dictionary<Tuple<TKey1, TKey2>, TValue>();
        }

        public void Fill(IEnumerable<TKey1> keys1, IEnumerable<TKey2> keys2, Func<TKey1, TKey2, TValue> generator)
        {
            /*
             * Nota: si verifichi il funzionamento del metodo Invoke() su oggetti di classe Func
             */
            foreach (TKey1 key1 in keys1)
                foreach (TKey2 key2 in keys2)
                    values.Add(new Tuple<TKey1, TKey2>(key1, key2), generator.Invoke(key1, key2)); 
        }

        public bool Equals(IMap2D<TKey1, TKey2, TValue> other)
        {
            if (other.NumberOfElements != this.NumberOfElements)
                return false;

            foreach(Tuple<TKey1,TKey2> k in values.Keys)
                if(!other[k.Item1, k.Item2].Equals(values[k]))
                    return false;

            return true;
        }

        public TValue this[TKey1 key1, TKey2 key2]
        {
            /*
             * Suggerimento: si noti che sulla classe Dictionary è definito un indicizzatore per i valori di chiave
             */
             
            get
            {
                if (values.ContainsKey(new Tuple<TKey1, TKey2>(key1, key2)))
                    return values[new Tuple<TKey1, TKey2>(key1, key2)];
                else
                    throw new ArgumentException();
            }

            set
            {
                if (values.ContainsKey(new Tuple<TKey1, TKey2>(key1, key2)))
                    values[new Tuple<TKey1, TKey2>(key1, key2)] = value;
                else
                    values.Add(new Tuple<TKey1, TKey2>(key1, key2),value);

            }
        }

        public IList<Tuple<TKey2, TValue>> GetRow(TKey1 key1)
        {
            List<Tuple<TKey2, TValue>> list = new List<Tuple<TKey2, TValue>>();

            foreach (var k in values.Keys)
                if (k.Item1.Equals(key1))
                    list.Add(new Tuple<TKey2, TValue>(k.Item2, values[k]));

            return list;
        }

        public IList<Tuple<TKey1, TValue>> GetColumn(TKey2 key2)
        {
            List<Tuple<TKey1, TValue>> list = new List<Tuple<TKey1, TValue>>();

            foreach (var k in values.Keys)
                if (k.Item2.Equals(key2))
                    list.Add(new Tuple<TKey1, TValue>(k.Item1, values[k]));

            return list;
        }

        public IList<Tuple<TKey1, TKey2, TValue>> GetElements()
        {
            List<Tuple<TKey1, TKey2, TValue>> list = new List<Tuple<TKey1, TKey2, TValue>>();
            foreach (var elem in values)
                list.Add(new Tuple<TKey1, TKey2, TValue>(elem.Key.Item1, elem.Key.Item2, elem.Value));

            return list;
        }

        public int NumberOfElements
        {
            get
            {
                int count = 0;
                foreach (var elem in values)
                    count++;

                return count;
            }
        }

        public override string ToString()
        {
            return base.ToString();
        }

        public override bool Equals(object obj)
        {
            return base.Equals(obj);
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }
    }
}
